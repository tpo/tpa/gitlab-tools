#!/usr/bin/python3

# Copyright (C) 2022 Jérôme Charaoui <lavamind@torproject.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import argparse
import logging
import os
import sys
from pprint import pprint

from datetime import datetime, timedelta, timezone
from dateutil.parser import parse as parse_datetime

from gitlab import Gitlab
from gitlab.exceptions import GitlabDeleteError, GitlabGetError, GitlabListError
import gitlab.config

PIPELINE_SOURCES = (
    "web",
    "schedule",
    "api",
    "external",
    "chat",
    "webide",
    "merge_request_event",
    "external_pull_request_event",
    "parent_pipeline",
    "trigger",
    "pipeline",
)


def format_datasize(b):
    """
    convert a number of bytes to a human-readable string
    copied from: https://gitlab.com/JonathonReinhart/gitlab-artifact-cleanup
    license: MIT
    """
    prefixes = ["", "Ki", "Mi", "Gi", "Ti"]

    for pref in prefixes:
        if (b < 1024) or (pref == prefixes[-1]):
            return "{:.02f} {}B".format(b, pref) if pref else "{} B".format(b)
        b /= 1024.0

    return None


def parse_timedelta(s):
    """
    convert a human-readable string representing a time delta into number of seconds
    copied from: https://gitlab.com/JonathonReinhart/gitlab-artifact-cleanup
    license: MIT
    """
    parts = s.split()
    n = int(parts[0])

    if len(parts) == 1:
        return timedelta(seconds=n)

    if len(parts) == 2:
        unit = parts[1].lower()

        # Allow singular units to be given
        if unit[-1] != "s":
            unit += "s"

        # Support units not supported by timedelta constructor
        if unit == "years":
            unit = "days"
            n *= 365
        elif unit == "months":
            unit = "days"
            n *= 30

        # Leverage timedelta kwargs for handling most units
        return timedelta(**{unit: n})

    raise ValueError


class BatchArgParser(argparse.ArgumentParser):
    """argument parsing helper"""

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.add_argument(
            "-q",
            "--quiet",
            action=LoggingAction,
            const="WARNING",
            help="Silence informational messages",
        )
        self.add_argument(
            "-d",
            "--debug",
            action=LoggingAction,
            const="DEBUG",
            help="Enable debugging messages",
        )
        self.add_argument(
            "-n",
            "--dry-run",
            action="store_true",
            help="Only show what would be removed, do not delete anything",
        )
        self.add_argument(
            "-i",
            "--instance",
            action="store",
            help="GitLab instance identifier from configuration file",
        )
        self.add_argument(
            "-c",
            "--config-file",
            action="store",
            help="Path to the python-gitlab configuration file",
        )
        self.add_argument(
            "-a",
            "--all-projects",
            action="store_true",
            help="Vacuum CI pipelines of all projects on the instance",
        )
        self.add_argument(
            "-p",
            "--project",
            action="append",
            dest="projects",
            help="Vacuum CI pipelines for one or more project (can be given multiple times)",
        )
        self.add_argument(
            "-r",
            "--ref",
            action="store",
            help="Limit vacuum to pipelines for specific ref (branch or tag)",
        )
        self.add_argument(
            "-s",
            "--source",
            action="store",
            type=self.check_source,
            help="Limit vacuum to pipelines of a specific source",
        )
        self.add_argument(
            "-l",
            "--keep-last",
            action="store",
            dest="keep_last",
            type=self.check_positive,
            help="Keep last n pipelines for each ref",
        )
        self.add_argument(
            "-m",
            "--min-age",
            type=parse_timedelta,
            default=timedelta(),
            help="Minimum pipeline age for deletion, in human-readable format"
            " (eg. '3 hours', '1 day', etc.; in seconds if suffix absent)",
        )
        self.add_argument(
            "--scrub-artifacts",
            action="store_true",
            help="Delete job artifacts in pipelines instead of deleting the pipelines entirely",
        )
        self.add_argument(
            "-t",
            "--keep-tags",
            action="store_true",
            help="Keep tag pipelines",
        )

    @staticmethod
    def check_positive(value):
        """validate positive integer"""
        ivalue = int(value)
        if ivalue <= 0:
            raise argparse.ArgumentTypeError(
                "--keep-last must be a positive integer" % value
            )
        return ivalue

    @staticmethod
    def check_source(value):
        """validate pipeline source"""
        if value not in PIPELINE_SOURCES:
            raise argparse.ArgumentTypeError("invalid --source specified" % value)
        return value

    def parse_args(self, *args, **kwargs):
        args = super().parse_args(*args, **kwargs)

        if args.config_file and not os.path.exists(args.config_file):
            self.error("%s does not exist" % args.config_file)

        return args


class LoggingAction(argparse.Action):
    """change log level on the fly

    The logging system should be initialized before this, using
    `basicConfig`.

    Example usage:

    parser.add_argument(
        "-v",
        "--verbose",
        action=LoggingAction,
        const="INFO",
        help="enable verbose messages",
    )
    parser.add_argument(
        "-d",
        "--debug",
        action=LoggingAction,
        const="DEBUG",
        help="enable debugging messages",
    )
    """

    def __init__(self, *args, **kwargs):
        """setup the action parameters

        This enforces a selection of logging levels. It also checks if
        const is provided, in which case we assume it's an argument
        like `--verbose` or `--debug` without an argument.
        """
        kwargs["choices"] = logging._nameToLevel.keys()
        if "const" in kwargs:
            kwargs["nargs"] = 0
        super().__init__(*args, **kwargs)

    def __call__(self, parser, ns, values, option_string=None):
        """if const was specified it means argument-less parameters"""
        if self.const:
            logging.getLogger("").setLevel(self.const)
        else:
            logging.getLogger("").setLevel(values)


class GitlabPipelineVacuum:
    """cleans up GitLab pipelines"""

    def __init__(self, args):
        self.dry_run = args.dry_run
        self.debug = args.debug
        self.scrub_artifacts = args.scrub_artifacts

        self.keep_tags = args.keep_tags
        self.filter = {
            "keep_last": args.keep_last,
            "keep_tags": args.keep_tags,
            "min_age": args.min_age,
        }
        self.query = {"ref": args.ref, "source": args.source}

        self.total = {
            "archive_size": 0,
            "trace_size": 0,
            "other_size": 0,
            "pipeline_count": 0,
            "project_count": 0,
        }
        self.now = datetime.now(timezone.utc)

        # load configuration
        try:
            self.gitlab = Gitlab.from_config(
                args.instance, [args.config_file] if args.config_file else None
            )
        except gitlab.config.ConfigError as cfgerr:
            print("Error loading python-gitlab config:", cfgerr)
            print(
                "See https://python-gitlab.readthedocs.io/en/stable/cli-usage.html#configuration-files"
            )
            sys.exit(1)

        # enable gitlab request debugging
        if self.debug:
            self.gitlab.enable_debug()

        # authenticate to GitLab instance
        self.gitlab.auth()
        logging.info("Successfully connected to %s", self.gitlab.url)

        # iterate over projects
        if args.all_projects:
            # all instance projects
            for proj in self.gitlab.projects.list(iterator=True):
                if (
                    proj.archived
                    or proj.attributes["builds_access_level"] == "disabled"
                ):
                    # skip archived projects or projects with CI disabled
                    continue
                self.vacuum_project(proj)
                self.total["project_count"] += 1
        else:
            # projects specified on command-line
            for pname in args.projects:
                proj = self.get_project(pname)
                if proj:
                    self.vacuum_project(proj)
                    self.total["project_count"] += 1

        self.vacuum_report()

    def get_project(self, pname):
        """retrieve a GitLab project"""
        try:
            project = self.gitlab.projects.get(pname)
            if logging.DEBUG >= logging.root.level:
                pprint(project.attributes)
            return project
        except GitlabGetError as gitlab_exception:
            logging.error("Error getting project %s (%s)", pname, gitlab_exception)
            return None

    def vacuum_project(self, proj):
        """vacuum all pipelines in a project"""

        logging.info(
            'Vacuuming project "%s" (%d) ...', proj.path_with_namespace, proj.id
        )

        # initialize variables
        self.project_keeping = {}

        try:
            self.project_tags = [t.name for t in proj.tags.list(iterator=True)]
        except GitlabListError:
            self.project_tags = []

        self.project_subtotal = {
            "archive_size": 0,
            "trace_size": 0,
            "other_size": 0,
            "pipeline_count": 0,
        }

        # filter using query parameters for efficiency
        query = {p: v for (p, v) in self.query.items() if v}

        # iterate over pipelines in project
        for pipeline in proj.pipelines.list(
            all=True,
            pagination="keyset",
            order_by="id",
            per_page=100,
            query_parameters=query,
        ):
            self.vacuum_pipeline(pipeline)

        # after processing all possible pipelines
        # show totals and breakdown of what will get deleted
        if self.project_subtotal["pipeline_count"] > 0:
            logging.info(
                "  %s %s (artifacts: %s / logs: %s / others: %s) in %s pipelines",
                "Would delete" if self.dry_run else "Deleted",
                format_datasize(
                    self.project_subtotal["archive_size"]
                    + self.project_subtotal["trace_size"]
                    + self.project_subtotal["other_size"]
                ),
                format_datasize(self.project_subtotal["archive_size"]),
                format_datasize(self.project_subtotal["trace_size"]),
                format_datasize(self.project_subtotal["other_size"]),
                self.project_subtotal["pipeline_count"],
            )

    def vacuum_pipeline(self, pipeline):
        """vacuum specific pipeline"""

        # output data about pipeline if --debug
        if logging.DEBUG >= logging.root.level:
            pprint(pipeline.attributes)

        # don't consider pending or running pipelines
        if pipeline.status in ("pending", "running"):
            logging.info(
                "  Pipeline %s for %s: skipped %s",
                pipeline.id,
                pipeline.ref,
                pipeline.status,
            )
            return

        # keep tag pipelines
        if self.filter["keep_tags"] and pipeline.ref in self.project_tags:
            logging.info("  Pipeline %s for %s: kept (tag)", pipeline.id, pipeline.ref)
            return

        # keep last n pipelines for any given ref in project
        if self.filter["keep_last"]:
            if pipeline.ref not in self.project_keeping or (
                pipeline.ref in self.project_keeping
                and self.project_keeping[pipeline.ref] < self.filter["keep_last"]
            ):
                logging.info(
                    "  Pipeline %s for %s: kept (in last %d)",
                    pipeline.id,
                    pipeline.ref,
                    self.filter["keep_last"],
                )
                if pipeline.ref in self.project_keeping:
                    self.project_keeping[pipeline.ref] += 1
                else:
                    self.project_keeping[pipeline.ref] = 1
                return

        # skip jobs that are too recent
        updated_ts = parse_datetime(pipeline.updated_at)
        age = self.now - updated_ts
        if age < self.filter["min_age"]:
            logging.info(
                "  Pipeline %s for %s: kept (too recent)", pipeline.id, pipeline.ref
            )
            return

        # get total size of artifacts per type
        # for all jobs in pipeline
        archive_size, trace_size, other_size = self.get_pipeline_stats(
            self.get_project(pipeline.project_id), pipeline.id
        )

        if self.scrub_artifacts:
            # delete artifacts in pipeline jobs, keeping logs around

            # format logging message bits
            if archive_size > 0:
                size_log = "archives %s" % (format_datasize(archive_size))
            else:
                size_log = "empty"

            # actually scrub unless --dry-run
            if not self.dry_run:
                self.scrub_pipeline_artifacts(pipeline)

        else:
            # delete entire pipeline including jobs, artifacts and logs

            # format logging message bits
            if archive_size + trace_size + other_size > 0:
                size_log = "archives %s / traces %s / other %s" % (
                    format_datasize(archive_size),
                    format_datasize(trace_size),
                    format_datasize(other_size),
                )
            else:
                size_log = "empty"

            # actually delete unless --dry-run
            if not self.dry_run:
                try:
                    pipeline.delete()
                except GitlabDeleteError as gitlab_exception:
                    logging.error(
                        "  Pipeline %s: delete failed: %s",
                        pipeline.id,
                        gitlab_exception,
                    )
                    return

        # log message
        logging.info(
            "  Pipeline %s for %s (%s): %s",
            pipeline.id,
            pipeline.ref,
            size_log,
            "would delete" if self.dry_run else "deleted",
        )

        # add to totals
        if archive_size + trace_size + other_size > 0:
            self.project_subtotal["archive_size"] += archive_size
            self.project_subtotal["trace_size"] += trace_size
            self.project_subtotal["other_size"] += other_size
            self.total["archive_size"] += archive_size
            self.total["trace_size"] += trace_size
            self.total["other_size"] += other_size

        self.project_subtotal["pipeline_count"] += 1
        self.total["pipeline_count"] += 1

    def get_pipeline_stats(self, proj, pid):
        """return 'artifact', 'trace' and 'other' filesizes for a pipeline"""

        archive_size = 0
        trace_size = 0
        other_size = 0

        pipeline = proj.pipelines.get(pid)
        for job in pipeline.jobs.list(iterator=True):
            a_size, t_size, o_size = self.get_job_stats(job)
            archive_size += a_size
            trace_size += t_size
            other_size += o_size

        return archive_size, trace_size, other_size

    @staticmethod
    def get_job_stats(job):
        """return 'artifact', 'trace' and 'other' filesizes for a job"""

        archive_size = 0
        trace_size = 0
        other_size = 0

        if "artifacts" in job.attributes:
            for af_file in job.attributes["artifacts"]:
                # grab sizes of the various artifact components
                if af_file["size"] is None:
                    continue
                if af_file["file_type"] == "archive":
                    archive_size += af_file["size"]
                elif af_file["file_type"] == "trace":
                    trace_size += af_file["size"]
                else:
                    other_size += af_file["size"]

        return archive_size, trace_size, other_size

    def scrub_pipeline_artifacts(self, pipeline):
        """delete artifacts for all jobs in pipeline"""
        for job in pipeline.jobs.list():
            archive_size, _, _ = self.get_job_stats(job)
            if archive_size > 0:
                try:
                    job.delete_artifacts()
                except AttributeError:
                    logging.warning("    Job %s: unable to delete artifacts!", job.id)

    def vacuum_report(self):
        """output short report about what was cleaned up"""

        if self.total["pipeline_count"] == 0:
            logging.info("No pipelines found in need of vacuuming.")
        else:
            if self.scrub_artifacts:
                size_log = "%s" % (format_datasize(self.total["archive_size"]))
            else:
                size_log = "%s (artifacts %s / logs %s / others %s)" % (
                    format_datasize(
                        self.total["archive_size"]
                        + self.total["trace_size"]
                        + self.total["other_size"]
                    ),
                    format_datasize(self.total["archive_size"]),
                    format_datasize(self.total["trace_size"]),
                    format_datasize(self.total["other_size"]),
                )
            logging.info(
                "%s a total of %s in %d projects and %d pipelines",
                "Would delete" if self.dry_run else "Deleted",
                size_log,
                self.total["project_count"],
                self.total["pipeline_count"],
            )


def main():
    """main entrypoint"""

    logging.basicConfig(format="%(message)s")
    logging.getLogger("").setLevel(logging.INFO)

    args = BatchArgParser().parse_args()

    GitlabPipelineVacuum(args)


if __name__ == "__main__":
    main()
