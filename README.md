⚠ WARNING: this project is deprecated. ⚠

Further code should be added in the fabric-tasks repository, in the
`fabric_tpa.gitlab` module, to favor code reuse and discoverability.

----

This repository contains a collection of tools to help with GitLab
administration.

# Vacuuming tools

Those tools are built to cleanup pipelines and artifacts from GitLab's
storage.

## Prerequisites

The scripts here depend on the python modules [python-gitlab][] and
[dateutil](https://dateutil.readthedocs.io/en/stable/).

They can be installed on Debian using `apt install python3-gitlab python3-dateutil`.

## Configuration

The `python-gitlab` module needs an INI-style configuration file that contains
the URL to one or more GitLab instances, as well as credentials.

The file is read from `~/.python-gitlab.cfg`, `/etc/python-gitlab.cfg` or can be
specified in the `PYTHON_GITLAB_CFG` environment variable or on the command-line
using the `--config-file` switch.

The configuration file looks like this:

    [global]
    default = torproject.org
    ssl_verify = true
    timeout = 10

    [torproject.org]
    url = https://gitlab.torproject.org
    private_token = <private token>
    api_version = 4

Details about the syntax and parameters of the configuration file can
be found in the [documentation][]. You can obtain a `private_token`
from the [personal access tokens][] page. Note that some (but not all)
scripts (e.g. `gitlab-tools`) also support getting the secret from the
`GITLAB_PRIVATE_TOKEN` environment variable.

[python-gitlab]: https://github.com/python-gitlab/python-gitlab
[documentation]: https://python-gitlab.readthedocs.io/en/stable/cli-usage.html#configuration-files
[personal access tokens]: https://gitlab.torproject.org/-/profile/personal_access_tokens

## gitlab-artifact-vacuum

This script assists in cleaning up stale CI artifacts in GitLab via the API,
while showing the amount of storage reclaimed.

### Usage

```
  -h, --help            show this help message and exit
  -q, --quiet           Silence informational messages
  -d, --debug           Enable debugging messages
  -n, --dry-run             Only show what would be removed, do not delete anything
  -i INSTANCE, --instance INSTANCE
                        GitLab instance identifier from configuration file
  -c CONFIG_FILE, --config-file CONFIG_FILE
                        Path to the python-gitlab configuration file
  -a, --all-projects    Vacuum CI artifacts of all projects on the instance
  -p PROJECTS, --project PROJECTS
                        Vacuum CI artifacts for one or more project (can be given multiple times)
  -j JOBS, --job JOBS   Vacuum CI artifacts for specific job IDs (single --project required, can be given multiple times)
  -e, --erase           Erase jobs including traces (logs) and other artifacts like coverage reports
  --include-not-expiring
                        Include CI jobs whose artifacts have no expiration set (eg. marked 'Keep')
  -m MIN_AGE, --min-age MIN_AGE
                        Minimum job age for artifacts to be deleted, in human-readable format (eg. '3 hours', '1 day', etc.; in seconds if suffix absent)
```

IMPORTANT: It's recommended to always first run a simulation using `--dry-run`.
The elements deleted using this script are not recoverable other than restoring
from backups!

### Examples

Remove all CI artifacts older than 30 days across the entire instance:

    gitlab-artifact-vacuum --all-projects --min-age "30 days"

Erase CI jobs older than 90 days across the entire instance, including artifacts
and traces (logs):

    gitlab-artifact-vacuum --all-projects --min-age "90 days" --erase

Note: the `--erase` switch does not completely purge jobs from the database:
their record will remain, including the result.

Erase all finished CI jobs for two projects:

    gitlab-artifact-vacuum --project group-one/project-a --project group-two-project-b

Remove artifacts for a single CI job:

    gitlab-artifacts-vacuum --project my-group/ciproject --job 99562

## gitlab-pipeline-vacuum

This script assists in cleaning up unwanted pipelines in GitLab via the API,
while showing the amount of storage reclaimed. When pipelines are deleted, any
associated artifacts and job logs are removed.

### Usage

```
  -h, --help            show this help message and exit
  -q, --quiet           Silence informational messages
  -d, --debug           Enable debugging messages
  -n, --dry-run         Only show what would be removed, do not delete anything
  -i INSTANCE, --instance INSTANCE
                        GitLab instance identifier from configuration file
  -c CONFIG_FILE, --config-file CONFIG_FILE
                        Path to the python-gitlab configuration file
  -a, --all-projects    Vacuum CI pipelines of all projects on the instance
  -p PROJECTS, --project PROJECTS
                        Vacuum CI pipelines for one or more project (can be given multiple times)
  -r REF, --ref REF     Limit vacuum to pipelines for specific ref (branch or tag)
  -s SOURCE, --source SOURCE
                        Limit vacuum to pipelines of a specific source
  -l KEEP_LAST, --keep-last KEEP_LAST
                        Keep last n pipelines for each ref
  -m MIN_AGE, --min-age MIN_AGE
                        Minimum pipeline age for deletion, in human-readable format (eg. '3 hours', '1 day', etc.; in seconds if suffix absent)
  --scrub-artifacts     Delete job artifacts in pipelines instead of deleting the pipelines entirely
  -t, --keep-tags       Keep tag pipelines
```

### Examples

Delete all instance pipelines older than 90 days:

    gitlab-pipeline-vacuum --all-projects --min-age "90 days"

Delete pipelines for two projects that are older than 1 week:

    gitlab-pipeline-vacuum --project group-one/project-a --project group-two-project-b --min-age "1 week"

Delete all pipelines for a single project, keeping the last pipeline for all
refs:

    gitlab-pipeline-vacuum --project my-group/ciproject --keep-last 1

Delete all pipelines older than 1 day, keeping the last 3 pipelines:

    gitlab-pipeline-vacuum --project my-group/ciproject --min-age "1 day" --keep-last 3

Delete all pipelines older than 6 hours on `dev` branch:

    gitlab-pipeline-vacuum --project my-group/ciproject --min-age "6 hours" --ref dev

## Notes

### Archived jobs

The GitLab API does not allow such jobs to be erased. If your GitLab instance
has job archiving enabled, `gitlab-artifacts-vacuum` with `--erase` will
encounter failures when attempting to delete archived jobs. In such cases, the
`gitlab-pipeline-vacuum` script may be used to delete the pipelines, or by using
the Rails console, see the [GitLab documentation][] for details.

[GitLab documentation]: https://docs.gitlab.com/ee/administration/job_artifacts.html#delete-job-artifacts-and-logs-from-jobs-completed-before-a-specific-date

## Credits

These tools include some parts of the [gitlab-artifact-cleanup][] project by
Jonathon Reinhart.

[gitlab-artifact-cleanup]: https://gitlab.com/JonathonReinhart/gitlab-artifact-cleanup
