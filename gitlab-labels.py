#!/usr/bin/python3

import logging

import argparse
from dataclasses import asdict, dataclass
import csv
import random
from typing import Optional

from gitlab import Gitlab
import gitlab.config


class LoggingAction(argparse.Action):
    """change log level on the fly

    The logging system should be initialized before this, using
    `basicConfig`.

    Example usage:

    parser.add_argument(
        "-v",
        "--verbose",
        action=LoggingAction,
        const="INFO",
        help="enable verbose messages",
    )
    parser.add_argument(
        "-d",
        "--debug",
        action=LoggingAction,
        const="DEBUG",
        help="enable debugging messages",
    )
    """

    def __init__(self, *args, **kwargs):
        """setup the action parameters

        This enforces a selection of logging levels. It also checks if
        const is provided, in which case we assume it's an argument
        like `--verbose` or `--debug` without an argument.
        """
        kwargs["choices"] = logging._nameToLevel.keys()
        if "const" in kwargs:
            kwargs["nargs"] = 0
        super().__init__(*args, **kwargs)

    def __call__(self, parser, ns, values, option_string=None):
        """if const was specified it means argument-less parameters"""
        if self.const:
            logging.getLogger("").setLevel(self.const)
        else:
            logging.getLogger("").setLevel(values)


class GitLabConnector:
    def __init__(self, instance, config_files, debug):
        self.debug = debug
        # load configuration
        try:
            self.gitlab = Gitlab.from_config(instance, config_files)
        except gitlab.config.ConfigError as cfgerr:
            logging.warning(
                "Error loading configuration: %s, see https://python-gitlab.readthedocs.io/en/stable/cli-usage.html#configuration-files for configuration instructions",  # noqa: E501
                cfgerr,
            )
            raise cfgerr

        # enable gitlab request debugging
        if debug:
            self.gitlab.enable_debug()

        # authenticate to GitLab instance
        self.gitlab.auth()
        logging.info("Successfully connected to %s", self.gitlab.url)

    def project_generator(self, projects):
        for project in projects or []:
            p = self.gitlab.projects.get(project)
            logging.debug("loaded project %s", p.attributes)
            yield p


@dataclass
class LabelMeta:
    name: str
    description: str = ""
    color: Optional[str] = None
    priority: Optional[int] = None


class GitlabLabelsArgumentParser(argparse.ArgumentParser):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.add_argument(
            "-q",
            "--quiet",
            action=LoggingAction,
            const="WARNING",
            help="Silence informational messages",
        )
        self.add_argument(
            "-d",
            "--debug",
            action=LoggingAction,
            const="DEBUG",
            help="Enable debugging messages",
        )
        self.add_argument(
            "-n",
            "--dry-run",
            action="store_true",
            help="Only show what would be removed, do not delete anything",
        )
        self.add_argument(
            "-i",
            "--instance",
            action="store",
            help="GitLab instance identifier from configuration file, default specified in configuration file",
        )
        self.add_argument(
            "-c",
            "--config-file",
            action="append",
            nargs="+",
            help="Path to the python-gitlab configuration file, default specified in https://python-gitlab.readthedocs.io/en/stable/cli-usage.html#configuration-files",  # noqa: E501
        )
        group = self.add_mutually_exclusive_group(required=True)
        group.add_argument(
            "-a",
            "--all-projects",
            action="store_true",
            help="work on all projects",
        )
        group.add_argument(
            "-p",
            "--project",
            action="append",
            dest="projects",
            help="which project to work on",
        )
        group.add_argument(
            "-g",
            "--group",
            action="append",
            dest="groups",
            help="which groups to work on",
        )
        self.add_argument(
            "-l",
            "--list",
            action="store_true",
            help="list labels in given project and exit",
        )
        self.add_argument(
            "--add-labels",
            nargs="+",
            help="add labels",
            default=[],
        )
        self.add_argument(
            "--add-labels-from-file",
            help="add labels from file",
        )
        self.add_argument(
            "--delete-labels",
            nargs="+",
            help="delete labels",
            default=[],
        )

    def parse_args(self, *args, **kwargs):
        args = super().parse_args(*args, **kwargs)
        if args.list and (args.add_labels or args.delete_labels):
            self.error("provide either labels or --list")
        args.add_labels = [LabelMeta(label) for label in args.add_labels]
        if args.add_labels_from_file:
            with open(args.add_labels_from_file) as fp:
                reader = csv.reader(fp)
                for row in reader:
                    args.add_labels.append(LabelMeta(*row))
        if not args.add_labels and not args.delete_labels:
            self.error("no add-labels or delete-labels provided")
        logging.debug("parsed arguments: %s", args)
        return args


def main():
    logging.basicConfig(format="%(levelname)s: %(message)s", level="INFO")
    parser = GitlabLabelsArgumentParser()
    args = parser.parse_args()
    gitlab_connector = GitLabConnector(args.instance, args.config_file, args.debug)
    gitlab = gitlab_connector.gitlab

    if args.all_projects:
        logging.info("loading all projects")
        objects = gitlab.projects.list(all=True)
    elif args.groups:
        logging.info("loading groups")
        objects = [gitlab.groups.get(group) for group in args.groups]
    elif args.projects:
        logging.info("loading projects")
        objects = gitlab_connector.project_generator(args.projects)
    else:
        parser.error("no project, group or all projects specified")

    if args.list:
        for obj in objects:
            logging.debug("object: %s", obj)
            for label in obj.labels.list(
                all=True, with_counts=True, include_ancestor_groups=False
            ):
                logging.debug("label: %s", label)
                if not getattr(label, "is_project_label", True):
                    logging.debug(
                        "skipping non-project label %s in project %s",
                        label.name,
                        obj.name,
                    )
                    continue
                print(label.name)
        return

    for obj in objects:
        process_labels(obj, args.add_labels, args.delete_labels, args.dry_run)


def process_labels(obj, add_labels, delete_labels, dry_run):
    known_labels = set()
    logging.debug("inspecting object %s", obj.name)
    for label in obj.labels.list(
        all=True, with_counts=True, include_ancestor_groups=False
    ):
        if not label.is_project_label:
            logging.debug("skipping non-project label %s in %s", label.name, obj.name)
            continue
        known_labels.add(label.name)
        if label.name in delete_labels:
            if dry_run:
                logging.warning("would delete label %s in %s", label.name, obj.name)
                continue
            logging.info("deleting label %s in %s", label.name, obj.name)
            obj.labels.delete(label.id)

    for label in add_labels:
        if label.name in known_labels:
            logging.info("label %s already exists in %s", label, obj.name)
            continue
        if not label.color:
            label.color = "#" + "".join(random.choices("0123456789ABCDEF", k=6))
            logging.debug("assigned random color %s", label.color)
        if dry_run:
            logging.warning("would create label %s in %s", label, obj.name)
            continue
        logging.info("creating label %s in %s", label, obj.name)
        obj.labels.create(asdict(label))


if __name__ == "__main__":
    main()
