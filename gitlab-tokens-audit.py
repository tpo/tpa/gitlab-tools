#!/usr/bin/python3

import argparse
import logging

from gitlab import Gitlab
import gitlab.exceptions


class LoggingAction(argparse.Action):
    """change log level on the fly

    The logging system should be initialized before this, using
    `basicConfig`.
    """

    def __init__(self, *args, **kwargs):  # type: ignore[no-untyped-def]
        """setup the action parameters

        This enforces a selection of logging levels. It also checks if
        const is provided, in which case we assume it's an argument
        like `--verbose` or `--debug` without an argument.
        """
        kwargs["choices"] = logging._nameToLevel.keys()
        if "const" in kwargs:
            kwargs["nargs"] = 0
        super().__init__(*args, **kwargs)

    def __call__(self, parser, ns, values, option):  # type: ignore[no-untyped-def]
        """if const was specified it means argument-less parameters"""
        if self.const:
            logging.getLogger("").setLevel(self.const)
        else:
            logging.getLogger("").setLevel(values)
        # cargo-culted from _StoreConstAction
        setattr(ns, self.dest, self.const or values)


class GitLabConnector:
    def __init__(self, instance, config_files, debug):
        self.debug = debug
        # load configuration
        try:
            self.gitlab = Gitlab.from_config(instance, config_files)
        except gitlab.config.ConfigError as cfgerr:
            logging.warning(
                "Error loading configuration: %s, see https://python-gitlab.readthedocs.io/en/stable/cli-usage.html#configuration-files for configuration instructions",  # noqa: E501
                cfgerr,
            )
            raise cfgerr

        # enable gitlab request debugging
        if debug:
            self.gitlab.enable_debug()

        # authenticate to GitLab instance
        self.gitlab.auth()
        logging.info("Successfully connected to %s", self.gitlab.url)

    def project_generator(self, projects):
        for project in projects or []:
            p = self.gitlab.projects.get(project)
            logging.debug("loaded project %s", p.attributes)
            yield p


class GitLabWikisArgumentParser(argparse.ArgumentParser):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.add_argument(
            "-q",
            "--quiet",
            action=LoggingAction,
            const="WARNING",
            help="Silence informational messages",
        )
        self.add_argument(
            "-d",
            "--debug",
            action=LoggingAction,
            const="DEBUG",
            help="Enable debugging messages",
        )
        self.add_argument(
            "-i",
            "--instance",
            action="store",
            help="GitLab instance identifier from configuration file, default specified in configuration file",
        )
        self.add_argument(
            "-c",
            "--config-file",
            action="append",
            nargs="+",
            help="Path to the python-gitlab configuration file, default specified in https://python-gitlab.readthedocs.io/en/stable/cli-usage.html#configuration-files",  # noqa: E501
        )
        self.add_argument(
            "-l",
            "--limit",
            help="limit inspection to N projects",
        )
        group = self.add_mutually_exclusive_group(required=True)
        group.add_argument(
            "-a",
            "--all-projects",
            action="store_true",
            help="work on all projects",
        )
        group.add_argument(
            "-p",
            "--project",
            action="append",
            dest="projects",
            help="which project to work on",
        )
        group.add_argument(
            "-g",
            "--group",
            action="append",
            dest="groups",
            help="which groups to work on",
        )

    def projects_iterator(self, args):
        try:
            gitlab_connector = GitLabConnector(
                args.instance, args.config_file, args.debug
            )
        except gitlab.exceptions.GitlabAuthenticationError as e:
            config_file_path = args.config_file or "~/.python-gitlab.cfg"
            self.error(
                "failed to login to GitLab: %s - maybe the token in %s expired?"
                % (e, config_file_path)
            )

        if args.all_projects:
            logging.info("loading all projects")
            for p in gitlab_connector.gitlab.projects.list(all=True, iterator=True):
                yield p
        elif args.groups:
            logging.info("loading groups")
            for group in args.groups:
                for group_obj in gitlab_connector.gitlab.groups.list(search=group):
                    yield gitlab_connector.gitlab.groups.get(group_obj.id)
                    for p in group_obj.projects.list(
                        archived=0, visibility="public", iterator=True
                    ):
                        yield gitlab_connector.gitlab.projects.get(p.id)
        elif args.projects:
            logging.info("loading projects")
            for p in gitlab_connector.project_generator(args.projects):
                yield p
        else:
            self.error("no project, group or all projects specified")


def list_project_variables(project):
    try:
        name = project.path_with_namespace
    except AttributeError:
        # groups
        name = project.path
    try:
        variables_list = project.variables.list()
    except gitlab.exceptions.GitlabError as e:
        if "403 Forbidden" in str(e):
            # special case: no CI/CD on that project
            logging.debug("no variables found in %s", name)
            return []
        logging.warning(
            "failed to fetch variables list for %s: %s",
            name,
            e,
        )
        return []
    if not variables_list:
        return []
    logging.info("%s variables: ", name)
    for variable in variables_list:
        logging.info(
            "variable: %s, masked: %s, protected: %s",
            variable.key,
            variable.masked,
            variable.protected,
        )
    return variables_list


def list_object_tokens(project):
    try:
        name = project.path_with_namespace
    except AttributeError:
        # groups
        name = project.path
    try:
        tokens_list = project.access_tokens.list()
    except gitlab.exceptions.GitlabError as e:
        logging.warning(
            "failed to fetch tokens list for %s: %s",
            name,
            e,
        )
        return []
    if not tokens_list:
        return []
    logging.info("%s access tokens: ", name)
    for token in tokens_list:
        logging.info(
            "token %s: created %s, expiry: %s, scope: %s",
            token.name,
            token.created_at,
            token.expires_at,
            token.scopes,
        )
    return tokens_list


def main():
    logging.basicConfig(format="%(levelname)s: %(message)s", level="INFO")
    parser = GitLabWikisArgumentParser()
    args = parser.parse_args()

    total_tokens = 0
    total_variables = 0
    for project_or_group in parser.projects_iterator(args):
        try:
            name = project_or_group.name_with_namespace
        except AttributeError:
            # groups
            name = project_or_group.name
        tokens = len(list_object_tokens(project_or_group))
        variables = len(list_project_variables(project_or_group))
        if tokens + variables:
            logging.info(
                "found %d tokens and %d variables in %s", tokens, variables, name
            )
        total_tokens += tokens
        total_variables += variables
    logging.info(
        "found %d tokens and %d variables total", total_tokens, total_variables
    )


if __name__ == "__main__":
    main()
